﻿using Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baccarat
{
    public class BaccaratHand : Hand
    {
        public int Points
        {
            get
            {
                int points = 0;

                foreach (Card card in hand)
                {
                    switch (card.Number)
                    {
                        case CardNumber.Ace:
                            points = (1 + points) % 10;
                            break;
                        case CardNumber.Two:
                            points = (2 + points) % 10;
                            break;
                        case CardNumber.Three:
                            points = (3 + points) % 10;
                            break;
                        case CardNumber.Four:
                            points = (4 + points) % 10;
                            break;
                        case CardNumber.Five:
                            points = (5 + points) % 10;
                            break;
                        case CardNumber.Six:
                            points = (6 + points) % 10;
                            break;
                        case CardNumber.Seven:
                            points = (7 + points) % 10;
                            break;
                        case CardNumber.Eight:
                            points = (8 + points) % 10;
                            break;
                        case CardNumber.Nine:
                            points = (9 + points) % 10;
                            break;
                        case CardNumber.Ten:
                        case CardNumber.Jack:
                        case CardNumber.Queen:
                        case CardNumber.King:
                            break;
                    }
                }
                return points;
            }
        }
        override public string ToString()
        {
            string s = string.Join(", ", hand);
            return (s += "\n\t" + Points + " points.");
        }
    }
}
