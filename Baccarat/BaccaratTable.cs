﻿using Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baccarat
{
    enum BaccaratBettingOptions
    {
        Banker, Player, Tie, Undecided
    }
    public class BaccaratTable : CardTable
    {
        private Card playerCardDrawn { get; set; }
        private BaccaratBettingOptions playerChoice { get; set; }
        public BaccaratTable(Player p) : base(p)
        {
            deck = new Deck();
            dealerHand = new BaccaratHand();
            playerHand = new BaccaratHand();
        }

        /// <summary>
        /// Start a new game with a new deck.
        /// </summary>
        override public void NewGame()
        {
            // Reset the table
            deck.NewDeck();
            deck.Shuffle();
        }

        public override void Start()
        {
            PlaceBets();
            ChooseWinner();

            dealerHand.NewHand();
            playerHand.NewHand();
            playerCardDrawn = null;

            // Give the player and dealer 2 cards
            playerHand.AddCard(deck.Draw());
            dealerHand.AddCard(deck.Draw());
            playerHand.AddCard(deck.Draw());
            dealerHand.AddCard(deck.Draw());

            PlayerDecisions();
            BankerDecisions();
            DeclareWinner();
        }
        private void PlayerDecisions()
        {
            PrintCards();
            // player doesnt draw if points > 6
            if (((BaccaratHand)playerHand).Points < 6)
            {
                Console.WriteLine("Player has less than 6 points. Drawing card.");
                // draw a card if 5 or less
                playerCardDrawn = deck.Draw();
                playerHand.AddCard(playerCardDrawn);
            }
            else
            {
                Console.WriteLine("Player has 6 or more points. Not drawing card.");
            }
            Console.WriteLine();
        }

        private void BankerDecisions()
        {
            PrintCards();
            var points = ((BaccaratHand)dealerHand).Points;
            // draw a card if player didnt draw a card and banker has 5 or less
            if (playerCardDrawn == null)
            {
                if (points <= 5)
                {
                    Console.WriteLine("Banker drew a card.");
                    dealerHand.AddCard(deck.Draw());
                }
                else
                {
                    Console.WriteLine("Banker did not draw a card.");
                }
            }
            // Player drew a card so act accordingly
            else
            {
                switch (points)
                {
                    // 2 or less draw a card
                    case 0:
                    case 1:
                    case 2:
                        Console.WriteLine("Banker drew a card.");
                        dealerHand.AddCard(deck.Draw());
                        break;
                    // Draw only if player drew a 8
                    case 3:
                        if (playerCardDrawn.Number == CardNumber.Eight)
                        {
                            Console.WriteLine("Banker drew a card.");
                            dealerHand.AddCard(deck.Draw());
                        }
                        else
                        {
                            Console.WriteLine("Banker did not draw a card.");
                        }
                        break;
                    // Draw only if player drew a 2/3/4/5/6/7
                    case 4:
                        if (playerCardDrawn.Number == CardNumber.Two ||
                            playerCardDrawn.Number == CardNumber.Three ||
                            playerCardDrawn.Number == CardNumber.Four ||
                            playerCardDrawn.Number == CardNumber.Five ||
                            playerCardDrawn.Number == CardNumber.Six ||
                            playerCardDrawn.Number == CardNumber.Seven)
                        {
                            Console.WriteLine("Banker drew a card.");
                            dealerHand.AddCard(deck.Draw());
                        }
                        else
                        {
                            Console.WriteLine("Banker did not draw a card.");
                        }
                        break;
                    // Draw only if player drew a 4/5/6/7
                    case 5:
                        if (playerCardDrawn.Number == CardNumber.Four ||
                            playerCardDrawn.Number == CardNumber.Five ||
                            playerCardDrawn.Number == CardNumber.Six ||
                            playerCardDrawn.Number == CardNumber.Seven)
                        {
                            Console.WriteLine("Banker drew a card.");
                            dealerHand.AddCard(deck.Draw());
                        }
                        else
                        {
                            Console.WriteLine("Banker did not draw a card.");
                        }
                        break;
                    // Draw only if player drew a 6/7
                    case 6:
                        if (playerCardDrawn.Number == CardNumber.Six ||
                            playerCardDrawn.Number == CardNumber.Seven)
                        {
                            Console.WriteLine("Banker drew a card.");
                            dealerHand.AddCard(deck.Draw());
                        }
                        else
                        {
                            Console.WriteLine("Banker did not draw a card.");
                        }
                        break;
                    // 7 or more do nothing
                    case 7:
                    case 8:
                    case 9:
                        Console.WriteLine("Banker did not draw a card.");
                        break;
                }
            }
            Console.WriteLine();
        }
        
        private void DeclareWinner()
        {
            PrintCards();
            if (((BaccaratHand)playerHand).Points > ((BaccaratHand)dealerHand).Points)
            {
                Console.WriteLine("Player Wins!");
                if (playerChoice == BaccaratBettingOptions.Player)
                {
                    this.AwardPlayer();
                }
            }
            else if (((BaccaratHand)playerHand).Points < ((BaccaratHand)dealerHand).Points)
            {
                Console.WriteLine("Banker Wins!");
                if (playerChoice == BaccaratBettingOptions.Banker)
                {
                    this.AwardPlayer();
                }
            }
            else
            {
                Console.WriteLine("Tie!");
                if(playerChoice == BaccaratBettingOptions.Tie)
                {
                    this.AwardPlayer();
                }
            }
            Console.WriteLine();
        }

        /// <summary>
        /// Print the player Cards.
        /// </summary>
        private void PrintCards()
        {
            Console.WriteLine("Player has " + playerHand.ToString());
            Console.WriteLine("Dealer has " + dealerHand.ToString());
        }


        /// <summary>
        /// Choose the winner of the match.
        /// </summary>
        private void ChooseWinner()
        {
            playerChoice = BaccaratBettingOptions.Undecided;
            char c = '0';
            do
            {
                Console.WriteLine("Choose hand to bet on.");
                Console.WriteLine("a) Player");
                Console.WriteLine("b) Banker");
                Console.WriteLine("c) Tie");
                c = Console.ReadKey().KeyChar;
                switch (c)
                {
                    case 'a':
                        playerChoice = BaccaratBettingOptions.Player;
                        break;
                    case 'b':
                        playerChoice = BaccaratBettingOptions.Banker;
                        break;
                    case 'c':
                        playerChoice = BaccaratBettingOptions.Tie;
                        break;
                }
            } while (playerChoice == BaccaratBettingOptions.Undecided);
            Console.WriteLine();
        }
    }
}
