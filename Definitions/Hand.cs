﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Definitions
{
    public abstract class Hand
    {
        protected List<Card> hand { get; set; }

        /// <summary>
        /// Clear the hand of previous cards.
        /// </summary>
        public void NewHand()
        {
            if (hand == null)
            {
                hand = new List<Card>();
            }
            else
            {
                hand.Clear();
            }
        }


        /// <summary>
        /// Add a card to the hand.
        /// </summary>
        /// <param name="c"></param>
        public void AddCard(Card c)
        {
            hand.Add(c);
        }

        override public string ToString()
        {
            return string.Join(", ", hand);
        }
    }
}
