﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Definitions
{
    public enum CardSuit { Diamonds, Clubs, Hearts, Spades }
    public enum CardNumber { Ace, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King }

    public class Card
    {
        private CardNumber number { get; set; }
        private CardSuit suit { get; set; }

        public Card(CardNumber number, CardSuit suit)
        {
            this.number = number;
            this.suit = suit;
        }

        public CardSuit Suit
        {
            get
            {
                return suit;
            }
        }

        public CardNumber Number
        {
            get
            {
                return number;
            }
        }

        override public string ToString()
        {
            return number + " of " + suit;
        }
    }
}
