﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Definitions
{
    public abstract class CardTable
    {
        protected Deck deck { get; set; }
        protected Hand dealerHand { get; set; }
        protected Hand playerHand { get; set; }
        abstract public void NewGame();
        abstract public void Start();
        protected Player player { get; set; }
        protected int wager { get; set; }
        
        public CardTable(Player p)
        {
            player = p;
        }

        /// <summary>
        /// Take bets from player.
        /// </summary>
        protected void PlaceBets()
        {
            wager = 0;
            do
            {
                try
                {
                    Console.WriteLine("You have " + player.Money + ".");
                    Console.WriteLine("Enter amount to wager:");
                    wager = Int32.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid entry. Enter an integer greater than 0 and less than " + player.Money + ".");
                }
            }
            while (!(wager > 0 && wager <= player.Money));

            player.Money -= wager;
        }

        /// <summary>
        /// Award player based on how much they wagered.
        /// </summary>
        protected void AwardPlayer()
        {
            player.Money += wager * 2;
        }
    }
}
