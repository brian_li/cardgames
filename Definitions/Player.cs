﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Definitions
{
    public class Player
    {
        private int money { get; set; }
        private const int StartingAmmount = 100;

        public Player()
        {
            money = StartingAmmount;
        }

        public int Money
        {
            get
            {
                return money;
            }
            set
            {
                money = value;
            }
        }
    }
}
