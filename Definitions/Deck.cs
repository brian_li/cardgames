﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Definitions
{
    public class Deck
    {
        private List<Card> cards { get; set; }

        public Deck()
        {
            NewDeck();
        }

        /// <summary>
        /// Create a new set of cards
        /// </summary>
        public void NewDeck()
        {
            if (cards == null)
            {
                cards = new List<Card>();
            }
            else
            {
                cards.Clear();
            }

            foreach (CardSuit suit in Enum.GetValues(typeof(CardSuit)))
            {
                foreach (CardNumber num in Enum.GetValues(typeof(CardNumber)))
                {
                    var card = new Card(num, suit);
                    cards.Add(card);
                }
            }
        }

        /// <summary>
        /// Remove a card from the deck
        /// </summary>
        /// <param name="num">Card number</param>
        /// <param name="suit">Card suit</param>
        public void RemoveCard(CardNumber num, CardSuit suit)
        {
            for (int i = 0; i < cards.Count; i++)
            {
                if (cards[i].Number == num && cards[i].Suit == suit)
                {
                    cards.RemoveAt(i);
                    break;
                }
            }
        }

        /// <summary>
        /// Return 
        /// </summary>
        public List<Card> Cards
        {
            get
            {
                return cards;
            }
        }


        /// <summary>
        /// Shuffle the deck.
        /// </summary>
        public void Shuffle()
        {
            // Shuffle the deck by selecting a random card placing it on the bottom of the deck
            Random r = new Random();
            for (int i = cards.Count - 1; i > 0; i--)
            {
                int n = r.Next(i + 1);
                var temp = cards[i];
                cards[i] = cards[n];
                cards[n] = temp;
            }
        }

        /// <summary>
        /// Draw from the top (array[0]) of the deck.
        /// </summary>
        /// <returns>The card at array[0]</returns>
        public Card Draw()
        {
            if (cards.Count == 0)
            {
                return null;
            }
            var card = cards[0];
            cards.Remove(card);
            return card;
        }
    }
}
