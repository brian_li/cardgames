﻿using BlackJack;
using Definitions;
using Spanish21;
using Baccarat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGames
{
    class Program
    {
        private static CardTable table { get; set; }
        private static Player player { get; set; }

        static void Main(string[] args)
        {
            char c;
            player = new Player();
            do
            {
                PrintMenu();
                c = Console.ReadKey().KeyChar;
                Console.WriteLine();
                PlayGame(c);
            } while (c != 'q' && player.Money > 0);

            if (player.Money <= 0)
            {
                Console.WriteLine("Sorry, you ran out of money.");
            }
            Console.WriteLine("Thank you for playing.");
            Console.ReadLine();
        }

        private static void PrintMenu()
        {
            Console.WriteLine("You have " + player.Money + " dolars.");
            Console.WriteLine("j) Play Blackjack.");
            Console.WriteLine("s) Play Spanish21.");
            Console.WriteLine("b) Play Baccarat.");
            Console.WriteLine("q) Quit.");
        }

        private static void PlayGame(char c)
        {
            switch (c)
            {
                case 'j':
                    Console.WriteLine("BlackJack");
                    table = new BlackJackTable(player);
                    table.NewGame();
                    table.Start();
                    break;
                case 's':
                    Console.WriteLine("Spanish 21");
                    table = new Spanish21Table(player);
                    table.NewGame();
                    table.Start();
                    break;
                case 'b':
                    Console.WriteLine("Baccarat");
                    table = new BaccaratTable(player);
                    table.NewGame();
                    table.Start();
                    break;
                default:
                    break;
            }
        }
    }
}
