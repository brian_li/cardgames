﻿using BlackJack;
using Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spanish21
{
    public class Spanish21Table : BlackJackTable
    {
        public Spanish21Table(Player p) : base(p) { }

        /// <summary>
        /// Start a new game with a new deck. 
        /// </summary>
        override public void NewGame()
        {
            // Reset the table
            deck.NewDeck();
            // remove all 10's since spanish 21 doesnt use 10's
            foreach (CardSuit suit in Enum.GetValues(typeof(CardSuit)))
            {
                deck.RemoveCard(CardNumber.Ten, suit);
            }
            deck.Shuffle();
        }

    }
}
