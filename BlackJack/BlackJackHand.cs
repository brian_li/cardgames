﻿using Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack
{
    public class BlackJackHand : Hand
    {
        public BlackJackHand()
        {
            NewHand();
        }


        /// <summary>
        /// Return the number of points this hand contains.
        /// </summary>
        public int Points
        {
            get
            {
                bool ace = false;
                int points = 0;

                foreach (Card card in hand)
                {
                    switch (card.Number)
                    {
                        case CardNumber.Ace:
                            ace = true;
                            points += 1;
                            break;
                        case CardNumber.Two:
                            points += 2;
                            break;
                        case CardNumber.Three:
                            points += 3;
                            break;
                        case CardNumber.Four:
                            points += 4;
                            break;
                        case CardNumber.Five:
                            points += 5;
                            break;
                        case CardNumber.Six:
                            points += 6;
                            break;
                        case CardNumber.Seven:
                            points += 7;
                            break;
                        case CardNumber.Eight:
                            points += 8;
                            break;
                        case CardNumber.Nine:
                            points += 9;
                            break;
                        case CardNumber.Ten:
                        case CardNumber.Jack:
                        case CardNumber.Queen:
                        case CardNumber.King:
                            points += 10;
                            break;
                    }
                }
                if (points <= 11 && ace)
                {
                    points += 10;
                }
                return points;
            }
        }


        /// <summary>
        /// Check if hand is busted
        /// </summary>
        /// <returns>True if greater than 21</returns>
        public bool IsBust()
        {
            return Points > 21;
        }

        /// <summary>
        /// Check if hand is blackjack (Ace + 10/jack/queen/king)
        /// </summary>
        /// <returns>True if blackjack</returns>
        public bool IsBlackJack()
        {
            return
                // Can only occur if hand has 2 cards
                hand.Count == 2
                &&
                // either first or second card is an Ace but not both
                ((hand[0].Number == CardNumber.Ace) ^ hand[1].Number == CardNumber.Ace)
                &&
                // either first or second card is 10/j/q/k but not both
                (hand[0].Number == CardNumber.Ten ||
                hand[0].Number == CardNumber.Jack ||
                hand[0].Number == CardNumber.Queen ||
                hand[0].Number == CardNumber.King)
                ^
                (hand[1].Number == CardNumber.Ten ||
                hand[1].Number == CardNumber.Jack ||
                hand[1].Number == CardNumber.Queen ||
                hand[1].Number == CardNumber.King);
        }

        override public string ToString()
        {
            string s = string.Join(", ", hand);
            return (s += "\n\t" + Points + " points.");
        }
    }
}

