﻿using Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack
{
    public class BlackJackTable : CardTable
    {
        public BlackJackTable(Player p) :base(p)
        {
            deck = new Deck();
            dealerHand = new BlackJackHand();
            playerHand = new BlackJackHand();
        }

        /// <summary>
        /// Start a new game with a new deck.
        /// </summary>
        override public void NewGame()
        {
            // Reset the table
            deck.NewDeck();
            deck.Shuffle();
        }

        override public void Start()
        {
            PlaceBets();
            dealerHand.NewHand();
            playerHand.NewHand();

            // Give the player and dealer 2 cards
            playerHand.AddCard(deck.Draw());
            dealerHand.AddCard(deck.Draw());
            playerHand.AddCard(deck.Draw());

            // this is the hole for the dealer
            Card c = deck.Draw();

            PlayerDecisions();
            // add the hole to dealers hand
            dealerHand.AddCard(c);
            DealerDecisions();
            DeclareWinner();
        }

        /// <summary>
        /// Player choices that effect their hand.
        /// </summary>
        private void PlayerDecisions()
        {
            // Auto win if blackjack
            if (((BlackJackHand)playerHand).IsBlackJack())
                return;

            char c;
            do
            {
                PrintMenu();
                c = Console.ReadKey().KeyChar;
                Console.WriteLine();
                if (c == 'h')
                {
                    Console.WriteLine();
                    Console.WriteLine("Player Hits.");
                    Card card = deck.Draw();
                    Console.WriteLine("Drew a " + card.ToString() + ".");
                    playerHand.AddCard(card);
                    Console.WriteLine();
                }
            } while (!((BlackJackHand)playerHand).IsBust() && c != 's');

            if (((BlackJackHand)playerHand).IsBust())
            {
                Console.WriteLine("Player Busted.");
            }
            else
            {
                Console.WriteLine("Player chose to stay.");
            }
            Console.WriteLine();
        }

        /// <summary>
        /// Print the player options.
        /// </summary>
        private void PrintMenu()
        {
            Console.WriteLine("Player has " + playerHand.ToString());
            Console.WriteLine("Dealer has " + dealerHand.ToString());
            Console.WriteLine("h to hit, s to stay");
        }

        /// <summary>
        /// Dealer choices that effect their hand.
        /// </summary>
        private void DealerDecisions()
        {
            // Dealer must play even if player busts
            // Dealer must hit until bust or over 17 points
            Console.WriteLine("Dealer has " + dealerHand.ToString());
            while (((BlackJackHand)dealerHand).Points < 17 && !((BlackJackHand)dealerHand).IsBust())
            {
                Console.WriteLine();
                Console.WriteLine("Dealer Hits.");
                Card card = deck.Draw();
                Console.WriteLine("Drew a " + card.ToString() + ".");
                dealerHand.AddCard(card);
                Console.WriteLine("Dealer has " + dealerHand.ToString());
                Console.WriteLine();
            }
            if (((BlackJackHand)dealerHand).IsBust())
            {
                Console.WriteLine("Dealer Busted.");
            }
            Console.WriteLine();
        }

        /// <summary>
        /// Declare the winner of this game.
        /// </summary>
        private void DeclareWinner()
        {
            Console.WriteLine("Player has " + playerHand.ToString());
            Console.WriteLine("Dealer has " + dealerHand.ToString());

            // Player only wins if blackjack
            if (((BlackJackHand)playerHand).IsBlackJack() ||
                // or if player didnt bust and dealer bust 
                (!((BlackJackHand)playerHand).IsBust() &&
                    (((BlackJackHand)dealerHand).IsBust() ||
                    // or if player has more points than dealer
                    ((BlackJackHand)playerHand).Points > ((BlackJackHand)dealerHand).Points)
                )
            )
            {
                Console.WriteLine("Player wins!");
                AwardPlayer();
            }
            else
            {
                Console.WriteLine("Dealer wins!");
            }
            Console.WriteLine();
        }
    }
}

